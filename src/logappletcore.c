/* appletcore.c
 *
 * Copyright (c) 2009 Adam Wick
 * Copyright (c) 2011-2012 Alexander Kojevnikov
 * Copyright (c) 2011 Dan Callaghan
 * Copyright (c) 2012 Ari Croock
 * Copyright (c) 2014 Florian Schmaus
 *
 * See COPYING for licensing information
 */

#include "logappletcore.h"

// TODO remove
#include <stdlib.h>
//#include <string.h>
#include <stdbool.h>
#include <errno.h>

#include <dbus/dbus-glib.h>

static GtkWidget* label;

void set_text(const char *msg) {
	gtk_label_set_markup(GTK_LABEL(label), msg);
}

static void signal_handler(DBusGProxy *obj, const char *msg, GtkWidget *widget) {
	set_text(msg);
}

// TODO non-static, why buf here? make optional ifdef DBUS
static void set_up_dbus_transfer() {
	DBusGConnection *connection;
	DBusGProxy *proxy;
	GError *error= NULL;

	connection = dbus_g_bus_get(DBUS_BUS_SESSION, &error);
	if(connection == NULL) {
		g_printerr("Failed to open connection: %s\n", error->message);
		g_error_free(error);
		exit(1);
	}

	proxy = dbus_g_proxy_new_for_name(
		connection,
		"eu.geekplace.LogApplet",
		"/eu/geekplace/LogApplet",
		"eu.geekplace.LogApplet");

	dbus_g_proxy_add_signal(proxy, "Update", G_TYPE_STRING, G_TYPE_INVALID);
	dbus_g_proxy_connect_signal(
		proxy, "Update", (GCallback)signal_handler, label, NULL);
}

static void read_from_fp(FILE *fp) {
	char* line;
	size_t size = 0;
	while (true) {
		size =  getdelim(&line, &size, 0, fp);
		if (size < 0) {
			perror("getdelim() failed");
			return;
		}
		set_text(line);
	}
}

#define LOG_APPLET_NULL_DELIM_SOURCE "log-applet/null-delim-source/nds"

static void set_up_command_source() {
	char* xdg_config_home = getenv("XDG_CONFIG_HOME");
	if (!xdg_config_home) {
		fprintf(stderr, "XDG_CONFIG_HOME not set");
		return;
	}
	GString* path = g_string_new(xdg_config_home);
	g_string_append_c(path, G_DIR_SEPARATOR);
	g_string_append(path, LOG_APPLET_NULL_DELIM_SOURCE);

	FILE* fp = popen(path->str, "r");
	if (!fp) {
		perror("popen() failed");
		return;
	}
	pthread_t thread;
	errno = pthread_create(&thread, NULL, read_from_fp, fp);
	if (errno) {
		perror("pthread_create() failed");
		return;
	}
}

GtkWidget* log_applet_init(void) {
	label = gtk_label_new("Waiting for initial log text...");
	gtk_label_set_ellipsize(GTK_LABEL(label), PANGO_ELLIPSIZE_END);

	gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
	set_up_dbus_transfer();
	set_up_command_source();
	return label;
}
