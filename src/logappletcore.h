/* appletcore.h
 *
 * Copyright (c) 2014 Florian Schmaus
 *
 * See COPYING for licensing information
 */


#ifndef _LOGAPPLETCORE_H_
#define _LOGAPPLETCORE_H_

#include <gtk/gtk.h>

GtkWidget* log_applet_init(void);

#endif /* _LOGAPPLETCORE_H_ */
