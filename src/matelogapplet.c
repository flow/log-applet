/* mateapplet.c
 *
 * Copyright (c) 2009 Adam Wick
 * Copyright (c) 2011-2012 Alexander Kojevnikov
 * Copyright (c) 2011 Dan Callaghan
 * Copyright (c) 2012 Ari Croock
 * Copyright (c) 2014 Florian Schmaus
 *
 * See COPYING for licensing information
 */

#include "logappletcore.h"
#include "config.h"

#include <stdlib.h>
#include <string.h>

#include <gtk/gtk.h>
#include <dbus/dbus-glib.h>

#include <mate-panel-applet.h>

static gboolean mate_log_applet_fill(MatePanelApplet *applet)
{
	mate_panel_applet_set_flags(
		applet,
		MATE_PANEL_APPLET_EXPAND_MAJOR |
		MATE_PANEL_APPLET_EXPAND_MINOR |
		MATE_PANEL_APPLET_HAS_HANDLE);

	mate_panel_applet_set_background_widget(applet, GTK_WIDGET(applet));

	log_applet_init();

	gtk_container_add(GTK_CONTAINER(applet), label);
	gtk_widget_show_all(GTK_WIDGET(applet));

	return TRUE;
}

static gboolean log_applet_factory(
	MatePanelApplet *applet, const gchar *iid, gpointer data)
{
	gboolean retval = FALSE;

	if(!strcmp(iid, "LogApplet"))
		retval = mate_log_applet_fill(applet);

	if(retval == FALSE) {
		printf("Wrong applet!\n");
		exit(-1);
	}

	return retval;
}

MATE_PANEL_APPLET_OUT_PROCESS_FACTORY(
	"LogAppletFactory",
	PANEL_TYPE_APPLET,
	"LogApplet",
	log_applet_factory,
	NULL);
